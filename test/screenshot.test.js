const webdriver = require('selenium-webdriver');
const chrome = require('selenium-webdriver/chrome');
const util = require('util');
const fs = require('fs');
const writeFile = util.promisify(fs.writeFile);
const path = require('path');
const { expect } = require('chai');

async function takeScreenshot(driver, file){
  let image = await driver.takeScreenshot();
  await writeFile(file, image, 'base64');
}

const screen = {
    width: 640,
    height: 480,
};

describe('screenshot', () => {
    const chromeOptions = ['--no-sandbox', '--disable-dev-shm-usage'];
    let driver = new webdriver.Builder()
        .forBrowser('chrome')
        .setChromeOptions(new chrome.Options().addArguments(chromeOptions).headless().windowSize(screen))
        .build();

    it('should fail this test on purpose', async function () {
        await driver.get('https://www.gitlab.com');
        await takeScreenshot(driver, path.join(process.env.OUTPUT_DIR || '.', 'gitlab.png'));
        this.test.attachments = ['gitlab.png'];
        const title = await driver.getTitle();

        expect(title).to.equal('Fail');
    }).timeout(5000);

    after(async () => driver.quit());
});